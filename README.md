# Introduction
This repository is meant to learn about libhandy. It is based
around the really nice guide [Building responsive apps for Linux Smartphones with GTK and libhandy, Part 1](https://tuxphones.com/tutorial-developing-responsive-linux-smartphone-apps-libhandy-gtk-part-1/). But instead of using python, I wanted to do it in C.

I will try to build upon the tutorial in order to get to know more of GTK and libhandy.
If this will only be useful to myself, so be it :)
