/* organizer-window.c
 *
 * Copyright 2020 Evangelos Ribeiro Tzaras
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "organizer-config.h"
#include "organizer-window.h"

struct _OrganizerWindow
{
  GtkApplicationWindow  parent_instance;

  HdySqueezer *squeezer;
  HdyViewSwitcher *headerbar_switcher;
  HdyViewSwitcherBar *bottom_switcher;
};

G_DEFINE_TYPE (OrganizerWindow, organizer_window, GTK_TYPE_APPLICATION_WINDOW)

static void
  on_headerbar_squeezer_notify (GtkContainer *squeezer,
                                GParamSpec   *spec)
{
  OrganizerWindow *window;
  GtkWidget *child;

  window = ORGANIZER_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (squeezer)));
  child = hdy_squeezer_get_visible_child (HDY_SQUEEZER (squeezer));

  hdy_view_switcher_bar_set_reveal (window->bottom_switcher,
                                    child == GTK_WIDGET (window->headerbar_switcher)
                                    ? FALSE : TRUE);
}

static void
organizer_window_class_init (OrganizerWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  /* We need to use any handy widget before loading the template,
   * otherwise GtkBuilder will complain about invalid types.
   */
  GtkWidget *bar = hdy_header_bar_new ();
  gtk_widget_class_set_template_from_resource (widget_class, "/org/tuxphones/Organizer/organizer-window.ui");
  gtk_widget_class_bind_template_child (widget_class, OrganizerWindow, squeezer);
  gtk_widget_class_bind_template_child (widget_class, OrganizerWindow, headerbar_switcher);
  gtk_widget_class_bind_template_child (widget_class, OrganizerWindow, bottom_switcher);
}

static void
organizer_window_init (OrganizerWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
  g_signal_connect (self->squeezer, "notify::visible-child",
                    G_CALLBACK (on_headerbar_squeezer_notify), NULL);
}
